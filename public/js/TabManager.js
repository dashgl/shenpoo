/*
 *   This file is part of Shenpoo.
 *   By: Kion, LemonHaze420 with contributions from PhilzYeah
 *
 *   Shenpoo is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Shenpoo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Shenpoo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

"use strict";

const TabManager = (function() {

	this.MEM = {}

	this.DOM = {
		tabs : {
			preview : document.getElementById('TabManager.tab.preview'),
			readout : document.getElementById('TabManager.tab.readout'),
			table : document.getElementById('TabManager.tab.table')
		},
		pages : {
			preview : document.getElementById('TabManager.page.preview'),
			readout : document.getElementById('TabManager.page.readout'),
			table : document.getElementById('TabManager.page.table')
		}
	}

	this.EVT = {
		handleTabClick : evt_handleTabClick.bind(this)
	}

	this.API = {
		openPage : api_openPage.bind(this)
	}

	init.apply(this);
	return this;

	function init() {
		
		this.DOM.tabs.preview.addEventListener('click', this.EVT.handleTabClick);
		this.DOM.tabs.readout.addEventListener('click', this.EVT.handleTabClick);
		this.DOM.tabs.table.addEventListener('click', this.EVT.handleTabClick);

		let leaf = localStorage.getItem('leaf') || 'preview';
		this.API.openPage(leaf);

	}

	function evt_handleTabClick(evt) {

		let elem = evt.target;
		let id = elem.getAttribute('id');
		let leaf = id.split('.').pop();
		this.API.openPage(leaf);
		
	}

	function api_openPage(leaf) {
		
		if(this.MEM.activeTab) {
			this.MEM.activeTab.classList.remove('active');
		}

		if(this.MEM.activePage) {
			this.MEM.activePage.classList.remove('open');
		}

		this.MEM.activeTab = this.DOM.tabs[leaf];
		this.MEM.activePage = this.DOM.pages[leaf];
		
		this.MEM.activeTab.classList.add('active');
		this.MEM.activePage.classList.add('open');

		localStorage.setItem('leaf', leaf);
	}

}).apply({});
