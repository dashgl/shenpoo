/*
 *   This file is part of Shenpoo.
 *   By: Kion, LemonHaze420 with contributions from PhilzYeah
 *
 *   Shenpoo is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Shenpoo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Shenpoo.  If not, see <https://www.gnu.org/licenses/>.
 *   
 */

"use strict";

const Mt5Names = {
	"Root" : { mt5 : 0 },
	"Body_Base" : { mt5 : 63 },
	"Spine" : { mt5 : 1 },
	"Hips" : { mt5 : 14 },
	"Right_Upper_Leg" : { mt5 : 16 },
	"Right_Lower_Leg" : { mt5 : 17 },
	"Right_Foot" : { mt5 : 18 },
	"Right_Foot_Toes" : { mt5 : 19 },
	"Left_Upper_Leg" : { mt5 : 21 },
	"Left_Lower_Leg" : { mt5 : 22 },
	"Left_Foot" : { mt5 : 23 },
	"Left_Foot_Toes" : { mt5 : 24 },
	"Right_Shoulder" : { mt5 : 4 },
	"Right_Upper_Arm" : { mt5 : 5 },
	"Right_Lower_Arm" : { mt5 : 6 },
	"Head" : { mt5 : 189 },
	"Hair" : { mt5 : 255 },
	"Right_Wrist" : { mt5 : 7 },
	"Right_Rigged_Hand" : { mt5 : 8 },
	"Right_Hand" : { mt5 : 191 },
	"Right_Hand_Index_Lower" : { mt5 : 28 },
	"Right_Hand_Index_Upper" : { mt5 : 29 },
	"Right_Hand_Thumb_Lower" : { mt5 : 25 },
	"Right_Hand_Thumb_Upper" : { mt5 : 26 },
	"Right_Hand_Fingers_Lower" : { mt5 : 31 },
	"Right_Hand_Fingers_Upper" : { mt5 : 32 },
	"Left_Shoulder" : { mt5 : 9 },
	"Left_Upper_Arm" : { mt5 : 10 },
	"Left_Lower_Arm" : { mt5 : 11 },
	"Left_Wrist" : { mt5 : 12 },
	"Left_Rigged_Hand" : { mt5 : 13 },
	"Left_Hand" : { mt5 : 190 },
	"Left_Hand_Index_Lower" : { mt5 : 43 },
	"Left_Hand_Index_Upper" : { mt5 : 44 },
	"Left_Hand_Thumb_Lower" : { mt5 : 40 },
	"Left_Hand_Thumb_Upper" : { mt5 : 41 },
	"Left_Hand_Fingers_Lower" : { mt5 : 46 },
	"Left_Hand_Fingers_Upper" : { mt5 : 47 },
	"Left_Jacket_Outer" : { mt5 : 183 },
	"Right_Jacket_Outer" : { mt5 : 182 },
	"Left_Jacket_Inner" : { mt5 : 89 },
	"Right_Jacket_Inner" : { mt5 : 88 },
	"Jaw" : { mt5 : 188 }
}

const MotnNames = {
	"Root" : {
		id : 0,
		x : "0",
		y : "63",
		z : "0",
		type : "pos",
		format : "FK"
	},
	"Body" : {
		id : 0,
		x : "63",
		y : "63",
		z : "63",
		type : "rot",
		format : "FK"
	},
	"Hips" : {
		id : 1,
		x : "14",
		y : "14",
		z : "14",
		type : "rot",
		format : "FK"
	},
	"Right Thigh" : {
		id : 5,
		x : "16",
		y : "16",
		z : "16",
		type : "rot",
		format : "FK"
	},
	"RFoot Target" : {
		id : 8,
		x : "16/17",
		y : "16/17",
		z : "16/17",
		type : "pos",
		format : "IK"
	},
	"Right Foot" : {
		id : 9,
		x : "18",
		y : "18",
		z : "18",
		type : "rot",
		format : "FK"
	},
	"Left Thigh" : {
		id : 12,
		x : "21",
		y : "21",
		z : "21",
		type : "rot",
		format : "FK"
	},
	"LFoot Target" : {
		id : 15,
		x : "21/22",
		y : "21/22",
		z : "21/22",
		type : "pos",
		format : "IK"
	},
	"Left Foot" : {
		id : 16,
		x : "23",
		y : "23",
		z : "23",
		type : "rot",
		format : "FK"
	},
	"Torso" : {
		id : 18,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"Torso Target" : {
		id : 20,
		x : "",
		y : "",
		z : "",
		type : "pos",
		format : ""
	},
	"Neck" : {
		id : 21,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"Head Lookat" : {
		id : 23,
		x : "",
		y : "",
		z : "",
		type : "pos",
		format : "IK"
	},
	"Left Shlder" : {
		id : 25,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"Left Elbow" : {
		id : 26,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"LHand Target" : {
		id : 29,
		x : "",
		y : "",
		z : "",
		type : "pos",
		format : "IK"
	},
	"Left Hand" : {
		id : 30,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"Right Shlder" : {
		id : 31,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"Right Elbow" : {
		id : 32,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	},
	"RHand Target" : {
		id : 35,
		x : "",
		y : "",
		z : "",
		type : "pos",
		format : "IK"
	},
	"Right Hand" : {
		id : 36,
		x : "",
		y : "",
		z : "",
		type : "rot",
		format : "FK"
	}
}
