/*
 *   This file is part of Shenpoo.
 *   By: Kion, LemonHaze420 with contributions from PhilzYeah
 *
 *   Shenpoo is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Shenpoo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Shenpoo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

"use strict";

const PreviewWidget = (function() {

	this.MEM = {
		clock : new THREE.Clock(),
		ik_count : -1
	}

	this.DOM = {
		pos : document.getElementById('pos'),
		frames : document.getElementById('frames'),
		section : document.getElementById('PreviewWidget.section'),
		expt : document.getElementById('PreviewWidget.expt')
	}

	this.EVT = {
		handleWindowResize : evt_handleWindowResize.bind(this),
		handleRowClick : evt_handleRowClick.bind(this),
		handleExportClick : evt_handleExportClick.bind(this)
	}

	this.API = {
		animate : api_animate.bind(this),
		loadModel : api_loadModel.bind(this),
		addLabels : api_addLabels.bind(this),
		addTargets : api_addTargets.bind(this),
		setFrameCount : api_setFrameCount.bind(this),
		setFrameState : api_setFrameState.bind(this),
		solveFootIK : api_solveFootIK.bind(this),
		solveHandIK : api_solveHandIK.bind(this),
		updateIkArmLeg : api_updateIkArmLeg.bind(this),
		applyLimitRotation : api_applyLimitRotation.bind(this),
		eulerCanonicalSet : api_eulerCanonicalSet.bind(this),
		wait : api_wait.bind(this),
		applyToGhost : api_applyToGhost.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		this.MEM.scene = new THREE.Scene();
		this.MEM.scene.background = new THREE.Color(0x3c3c3c);

		this.MEM.renderer = new THREE.WebGLRenderer({
			antialias: true
		});

		this.MEM.renderer.setPixelRatio(window.devicePixelRatio);
		this.DOM.section.appendChild(this.MEM.renderer.domElement);

		this.MEM.camera = new THREE.PerspectiveCamera(60, 1, 0.01, 10000);
		this.MEM.camera.position.set(-1, 0.8, -2);
		this.MEM.camera.lookAt(new THREE.Vector3(0,1,0));
		this.EVT.handleWindowResize();

		this.MEM.controls = new THREE.OrbitControls(this.MEM.camera, this.DOM.section);
		this.MEM.controls.enableDamping = true;
		this.MEM.controls.dampingFactor = 0.25;
		this.MEM.controls.target = new THREE.Vector3( 0, 1, 0 );
		this.MEM.controls.screenSpacePanning = false;

		let grid = new THREE.GridHelper(10, 10, 0xff0000, 0xffffff);
		this.MEM.scene.add(grid);
		// this.MEM.scene.add(new THREE.AmbientLight(0xcccccc));
		
		const aLight = new THREE.DirectionalLight( 0xffffff, 0.9 );
		aLight.position.x = -5;
		aLight.position.y = 3;
		aLight.position.z = 3;
		this.MEM.scene.add( aLight );
		
		window.addEventListener('resize', this.EVT.handleWindowResize);

		this.API.animate();
		this.API.loadModel();
		this.API.addLabels();
		this.API.addTargets();

		this.DOM.frames.addEventListener('click', this.EVT.handleRowClick);
		this.DOM.expt.addEventListener('click', this.EVT.handleExportClick);

	}

	async function evt_handleExportClick() {

		for(let i = 0; i < this.DOM.frames.cells.length; i++) {
			this.DOM.frames.cells[i].click();
			await this.API.wait();
		}
		
	}

	function api_wait() {
		
		return new Promise( (resolve, reject) => {

			setTimeout( () => {
				resolve();
			}, 100);

		});
	}

	function api_addTargets() {

		const sphere = new THREE.OctahedronBufferGeometry( 0.08, 0);

		let mat1 = new THREE.MeshBasicMaterial( { color: 0x00ce00 } );
		let light1 = new THREE.Mesh( sphere,  mat1);
		this.MEM.rFootTarget = light1;
		this.MEM.scene.add(light1);

		let mat2 = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
		let light2 = new THREE.Mesh( sphere,  mat2);
		this.MEM.lFootTarget = light2;
		this.MEM.scene.add(light2);

		let light3 = new THREE.Mesh( sphere,  mat1);
		this.MEM.rHandTarget = light3;
		this.MEM.scene.add(light3);

		let light4 = new THREE.Mesh( sphere,  mat2);
		this.MEM.lHandTarget = light4;
		this.MEM.scene.add(light4);

		this.MEM.log = [];
	}

	function api_addLabels() {

		let ng = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const nm = new THREE.MeshBasicMaterial( {color: 0xffff00} );
		const north = new THREE.Mesh( ng, nm );
		north.position.z = -2;
		north.position.y = 0.1;
		this.MEM.scene.add(north);

		let sg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const sm = new THREE.MeshBasicMaterial( {color: 0x0000ff} );
		const south = new THREE.Mesh( sg, sm );
		south.position.z = 2;
		south.position.y = 0.1;
		this.MEM.scene.add(south);

		let wg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const wm = new THREE.MeshBasicMaterial( {color: 0xff0000} );
		const west = new THREE.Mesh( wg, wm );
		west.position.x = -2;
		west.position.y = 0.1;
		this.MEM.scene.add(west);

		let eg = new THREE.SphereBufferGeometry( 0.05, 32, 32 );
		const em = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
		const east = new THREE.Mesh( eg, em );
		east.position.x = 2;
		east.position.y = 0.1;
		this.MEM.scene.add(east);

	}

	function evt_handleWindowResize() {

		let width = window.innerWidth - 400;
		let height = window.innerHeight - 72;
		this.MEM.renderer.setSize(width, height);

		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();

	}

	function api_loadModel() {

		// First Load the textures (lazy)

		let mats = [];
		let omats = [];
		for(let i = 0; i < 10; i++) {
			let path = `assets/ryo_0${i}.png`;
			let texture = new THREE.TextureLoader().load(path);
			let material = new THREE.MeshBasicMaterial({
				map : texture,
				skinning : true
			});
			mats.push(material);
			
			material = new THREE.MeshBasicMaterial({
				transparent : true,
				opacity : 0.5,
				map : texture,
				skinning : true
			});
			omats.push(material);
		}

		this.MEM.mats = mats;

		// Then we go ahead and load in the binary

		let ajax = new XMLHttpRequest();
		ajax.open('GET', 'assets/ryo.mt5');
		ajax.responseType = 'arraybuffer';
		ajax.send();

		ajax.onload = () => {
			
			// Load Animation Data

			let data = ajax.response;
			this.MEM.data = data;

			// Load Non-Prerotation

			let loader = new Mt5Loader(mats, data);
			loader.remove_rot = true;
			let mesh = loader.parse();

			let skelHelper = new THREE.SkeletonHelper(mesh);
			skelHelper.material.lineWidth = 5;
			this.MEM.scene.add(skelHelper);
			this.MEM.scene.add(mesh);
			this.MEM.mesh = mesh;
			this.MEM.bones = mesh.skeleton.bones;
			this.MEM.bones = loader.skel;

			// Load Rotation Values

			loader = new Mt5Loader(omats, data);
			loader.remove_rot = false;
			mesh = loader.parse();
			mesh.position.y = 3;

			skelHelper = new THREE.SkeletonHelper(mesh);
			skelHelper.material.lineWidth = 5;
			this.MEM.scene.add(skelHelper);
			this.MEM.scene.add(mesh);
			this.MEM.ghost = mesh;
			this.MEM.gbones = mesh.skeleton.bones;
			
			this.MEM.ghost.userData = this.MEM.ghost.userData || {};
			this.MEM.ghost.userData.quats = [];
			for(let i = 0; i < this.MEM.gbones.length; i++) {
				let b = this.MEM.gbones[i];
				let q = b.quaternion.toArray();
				this.MEM.ghost.userData.quats.push(q);
			}

			// Load Walk Animation

			SelectWidget.API.loadAnimation(242);
			this.MEM.rightFoot = [];

		}

	}

	function api_animate() {

		window.requestAnimationFrame(this.API.animate);

		let delta = this.MEM.clock.getDelta();
		if(this.MEM.mixer) {
			this.MEM.mixer.update(delta);
		}

		if(this.MEM.ik_count > 0) {
			this.MEM.ik_count--;
			this.API.solveFootIK('right');
			this.API.solveFootIK('left');
			this.API.solveHandIK('left');
			this.API.solveHandIK('right');
		} else if(this.MEM.ik_count === 0) {
			this.MEM.ik_count = -1;
			// this.API.applyToGhost();
		}

		this.MEM.controls.update();
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

	}

	function api_applyToGhost() {

		// First we reset all of the rotations back to the 'T-Pose'
		
		for(let i = 0; i < this.MEM.ghost_bones.length; i++) {
			let b = this.MEM.ghost_bones[i];
			let q = this.MEM.ghost.userData.quats[i];
			b.quaternion.fromArray(q);
		}

		// Then we go ahead and apply the solved IK to the ghost rig

		let src = this.MEM.mesh.skeleton.bones;
		let dst = this.MEM.ghost.skeleton.bones;
		
		console.log(src[30]);
		console.log(src[30].rotation);
		let q = src[30].quaternion.clone();
		let c = src[30].quaternion.clone();
		
		let b = dst[30];
		b = b.parent;
		while(b && b.parent) {
			q.multiply(b.quaternion);
			b = b.parent;
		}
		
		console.log(q);
		console.log(c);

		dst[30].quaternion.multiply(q);

	}

	function evt_handleRowClick(evt) {

		let elem = evt.target;
		if(elem.tagName !== 'TD') {
			return;
		}

		let p = elem.textContent;
		let transform = `pos f${p}`;
		this.DOM.pos.setAttribute('class', transform);

		let frameData = TableWidget.API.getFrameData(parseInt(p));
		this.API.setFrameState(frameData);

	}

	function api_setFrameCount(count) {

		this.DOM.frames.innerHTML = '';
		for(let i = 0; i < count; i++) {
			let c = this.DOM.frames.insertCell();
			c.textContent = i;
		}

		this.DOM.pos.style.width = `calc(100% / ${count})`;
		let transform = `pos f0`;
		this.DOM.pos.setAttribute('class', transform);

	}

	function angleToQuat(angles) {
		
		let m = new THREE.Matrix4();
		for(let axis in angles) {
			let mat = new THREE.Matrix4();
			switch(axis) {
			case 'x':
				mat.makeRotationX(angles[axis]);
				break;
			case 'y':
				mat.makeRotationY(angles[axis]);
				break;
			case 'z':
				mat.makeRotationZ(angles[axis]);
				break;
			}
			m.multiply(mat);
		}

		let quat = new THREE.Quaternion();
		quat.setFromRotationMatrix(m);
		return quat;

	}

	function api_setFrameState(frameData) {
		
		this.MEM.frameData = frameData;

		// Apply to Ghost

		console.log(frameData);
		
		let quat = angleToQuat(frameData['motn_5'].rot);
		console.log(this.MEM.gbones[30]);
		this.MEM.gbones[30].quaternion.copy(quat);

		let b = this.MEM.gbones[30]; 
		while(b) {
			console.log(b.name);
			console.log(b.userData.rot);
			console.log("---");
			b = b.parent;
		}
		// Apply to IK Rig

		frameData['motn_0'].pos.x = frameData['motn_0'].pos.x || 0;
		this.MEM.bones[1].position.y = frameData['motn_0'].pos.y;
		// this.MEM.bones[0].position.z = frameData['motn_0'].pos.z;
		
		// Right foot target

		frameData['motn_8'].pos.z -= frameData['motn_0'].pos.z;
		this.MEM.rFootTarget.position.x = frameData['motn_8'].pos.x;
		this.MEM.rFootTarget.position.y = frameData['motn_8'].pos.y;
		this.MEM.rFootTarget.position.z = frameData['motn_8'].pos.z;
		
		// Left Foot Target
		
		frameData['motn_15'].pos.z -= frameData['motn_0'].pos.z;
		this.MEM.lFootTarget.position.x = frameData['motn_15'].pos.x;
		this.MEM.lFootTarget.position.y = frameData['motn_15'].pos.y;
		this.MEM.lFootTarget.position.z = frameData['motn_15'].pos.z;
		
		// Left Hand Target

		frameData['motn_29'].pos.z -= frameData['motn_0'].pos.z;
		this.MEM.lHandTarget.position.x = frameData['motn_29'].pos.x;
		this.MEM.lHandTarget.position.y = frameData['motn_29'].pos.y;
		this.MEM.lHandTarget.position.z = frameData['motn_29'].pos.z;
		
		// Right Hand Target
		
		frameData['motn_35'].pos.z -= frameData['motn_0'].pos.z;
		this.MEM.rHandTarget.position.x = frameData['motn_35'].pos.x;
		this.MEM.rHandTarget.position.y = frameData['motn_35'].pos.y;
		this.MEM.rHandTarget.position.z = frameData['motn_35'].pos.z;

		this.MEM.ik_count = 20;

	}

	/*
	 * Reference:
	 * https://github.com/mrdoob/three.js/blob/master/examples/jsm/animation/CCDIKSolver.js
	 * https://yosipy.github.io/Threejs_IK_Sample/
	 * http://www.darwin3d.com/gdm1998.htm
	 */

	function api_solveFootIK(dir) {
		
		let thigh, knee, foot, target;
		// We'll use the bone index for now

		switch(dir) {
		case 'right':
			thigh = this.MEM.bones[30];
			knee = this.MEM.bones[31];
			foot = this.MEM.bones[32];
			target = this.MEM.rFootTarget;
			break;
		case 'left':
			thigh = this.MEM.bones[34];
			knee = this.MEM.bones[35];
			foot = this.MEM.bones[36];
			target = this.MEM.lFootTarget;
			break;
		}
			
		this.API.updateIkArmLeg(target, foot);

	}

	function api_solveHandIK(dir) {
		
		let shoulder, elbow, hand, target;
		// We'll use the bone index for now

		switch(dir) {
		case 'right':
			shoulder = this.MEM.bones[6];
			elbow = this.MEM.bones[7];
			hand = this.MEM.bones[8];
			target = this.MEM.rHandTarget;
			break;
		case 'left':
			shoulder = this.MEM.bones[16];
			elbow = this.MEM.bones[17];
			hand = this.MEM.bones[18];
			target = this.MEM.lHandTarget;
			break;
		}
		
		this.API.updateIkArmLeg(target, hand);

	}

	function api_updateIkArmLeg(ik, child_bone) {
	
		let destination = ik.position.clone();
		destination = child_bone.parent.worldToLocal(destination);
		let prevent_tip = new THREE.Vector3();
		prevent_tip.setFromMatrixPosition(child_bone.matrixWorld);
		prevent_tip = child_bone.parent.worldToLocal(prevent_tip);
	
		let axis = new THREE.Vector3();
		axis.crossVectors( prevent_tip, destination ).normalize();
		let angle = Math.acos( prevent_tip.dot(destination) / (prevent_tip.length() * destination.length()) );
	
		if( (axis.x !== 0 || axis.y !== 0 || axis.z !== 0 ) &&
			( !isNaN(axis.x) && !isNaN(axis.y) && !isNaN(axis.z) && !isNaN(angle) ) ){

			let parent = child_bone.parent;
		
			let quaternion = new THREE.Quaternion();
			quaternion.setFromAxisAngle( axis, angle );
			parent.quaternion.multiply( quaternion );

			// setting rotation limit
			this.API.applyLimitRotation(parent);

		}

		child_bone.updateMatrixWorld();
		child_bone.parent.updateMatrixWorld();
		
		destination = ik.position.clone();
		let direction = new THREE.Vector3();
		direction.setFromMatrixPosition(child_bone.parent.matrixWorld);
		direction.sub(new THREE.Vector3().setFromMatrixPosition(child_bone.matrixWorld));
		destination.add(direction);

		destination = child_bone.parent.parent.worldToLocal(destination);
		prevent_tip.setFromMatrixPosition(child_bone.parent.matrixWorld);
		prevent_tip = child_bone.parent.parent.worldToLocal(prevent_tip);

		axis.crossVectors( prevent_tip, destination ).normalize();
		angle = Math.acos( prevent_tip.dot(destination) / (prevent_tip.length() * destination.length()) );

		if( (axis.x !== 0 || axis.y !== 0 || axis.z !== 0 ) &&
			( !isNaN(axis.x) && !isNaN(axis.y) && !isNaN(axis.z) && !isNaN(angle) ) ){

			let grand_parent = child_bone.parent.parent;

			let quaternion = new THREE.Quaternion();
			quaternion.setFromAxisAngle( axis, angle * 0.2 );
			grand_parent.quaternion.multiply( quaternion );

			// setting rotation limit
			this.API.applyLimitRotation(grand_parent);
			
		}

		if( !(child_bone.name === 'Left_Hand' || child_bone.name === 'Right_Hand') ){
			return;
		}

		child_bone.parent.updateMatrixWorld();
		child_bone.parent.parent.updateMatrixWorld();

		destination = ik.position.clone();
		// grand child bone direction
		direction.setFromMatrixPosition(child_bone.parent.matrixWorld);
		direction.sub(new THREE.Vector3().setFromMatrixPosition(child_bone.matrixWorld));
		destination.add(direction);

		direction.setFromMatrixPosition(child_bone.parent.parent.matrixWorld);
		direction.sub(new THREE.Vector3().setFromMatrixPosition(child_bone.parent.matrixWorld));
		destination.add(direction);

		destination = child_bone.parent.parent.parent.worldToLocal(destination);
		prevent_tip.setFromMatrixPosition(child_bone.parent.parent.matrixWorld);
		prevent_tip = child_bone.parent.parent.parent.worldToLocal(prevent_tip);

		axis.crossVectors( prevent_tip, destination ).normalize();
		angle = Math.acos( prevent_tip.dot(destination) / (prevent_tip.length() * destination.length()) );

		if( (axis.x !== 0 || axis.y !== 0 || axis.z !== 0 ) &&
			( !isNaN(axis.x) && !isNaN(axis.y) && !isNaN(axis.z) && !isNaN(angle) ) ){

			let great_grand_parent = child_bone.parent.parent.parent;

			let quaternion = new THREE.Quaternion();
			quaternion.setFromAxisAngle( axis, angle * 0.05 );
			great_grand_parent.quaternion.multiply( quaternion );

			// setting rotation limit
			this.API.applyLimitRotation(great_grand_parent);

		}


	}

	function api_applyLimitRotation(bone) {

		let euler = new THREE.Euler();
		let quaternion = new THREE.Quaternion();
		
		if( bone.name === 'Left_Shoulder' ){
			euler.setFromQuaternion(bone.quaternion, 'XZY', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y > Math.PI/6 ){
				euler.y = Math.PI/6;
			}
			else if ( euler.y < -Math.PI/6 ){
				euler.y = -Math.PI/6;
			}

			euler.x = 0;

			if( euler.z < -Math.PI * 45/180 ){
				euler.z = -Math.PI * 45/180;
			}
			else if( euler.z > 0 ){
				euler.z = 0;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}

		if( bone.name === 'Right_Shoulder' ){
			euler.setFromQuaternion(bone.quaternion, 'XZY', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y < -Math.PI/6 ){
				euler.y = -Math.PI/6;
			}
			else if ( euler.y > Math.PI/6 ){
				euler.y = Math.PI/6;
			}

			euler.x = 0;

			if( euler.z > Math.PI * 45/180 ){
				euler.z = Math.PI * 45/180;
			}
			else if( euler.z < 0 ){
				euler.z = 0;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}



		if( bone.name === 'Left_Upper_Arm' ){
			euler.setFromQuaternion(bone.quaternion, 'YXZ', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y < -Math.PI * 160/180 ){
				euler.y = -Math.PI * 160/180;
			}
			else if ( euler.y > 0 ){
				euler.y = 0;
			}

			if( euler.x > Math.PI/2){
				euler.x = Math.PI/2;
			}
			else if( euler.x < -Math.PI/2){
				euler.x = -Math.PI/2;
			}

			if( euler.z > Math.PI *120/180 ){
				euler.z = Math.PI *120/180;
			}
			else if( euler.z < -Math.PI *50/180 ){
				euler.z = -Math.PI *50/180;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}

		if( bone.name === 'Left_Lower_Arm' ){
			euler.setFromQuaternion(bone.quaternion, 'YZX', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y < -Math.PI * 170/180 ){
				euler.y = -Math.PI * 170/180 ;
			}
			else if ( euler.y > 0 ){
				euler.y = 0;
			}
			euler.z = 0;
			//euler.x = 0;
			if( euler.x < 0 ){
				euler.x = 0;
			}else if( euler.x > Math.PI/4 ){
				euler.x = Math.PI/4;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}

		if( bone.name === 'Right_Upper_Arm' ){
			euler.setFromQuaternion(bone.quaternion, 'YXZ', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y > Math.PI * 160/180 ){
				euler.y = Math.PI * 160/180;
			}
			else if ( euler.y < 0 ){
				euler.y = 0;
			}

			if( euler.x < -Math.PI/2){
				euler.x = -Math.PI/2;
			}
			else if( euler.x > Math.PI/2){
				euler.x = Math.PI/2;
			}

			if( euler.z < -Math.PI *120/180 ){
				euler.z = -Math.PI *120/180;
			}
			else if( euler.z >Math.PI *50/180 ){
				euler.z = Math.PI *50/180;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}

	
		if( bone.name === 'Right_Lower_Arm' ){

			euler.setFromQuaternion(bone.quaternion, 'YZX', true);
			this.API.eulerCanonicalSet(euler);
			if( euler.y > Math.PI * 170/180 ){
				euler.y = Math.PI * 170/180 ;
			}
			else if ( euler.y < 0 ){
				euler.y = 0;
			}
			euler.z = 0;
			//euler.x = 0;
			if( euler.x < 0 ){
				euler.x = 0;
			} else if( euler.x > Math.PI/4 ){
				euler.x = Math.PI/4;
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);

		}

		if( bone.name === 'Right_Upper_leg' || bone.name === 'Right_Upper_Leg' ){

			euler.setFromQuaternion(bone.quaternion, 'XZY', true);
			this.API.eulerCanonicalSet(euler);

			if( euler.y < -Math.PI/2 ){
				euler.y = -Math.PI/2;
			}
			else if( euler.y > Math.PI/2){
				euler.y = Math.PI/2;
			}

			// left
			if( bone.name === 'Left_Upper_Leg' ){
				if( euler.z > Math.PI/6 ){
					euler.z = Math.PI/6;
				}
				else if( euler.z < -5/6 * Math.PI){
					euler.z = -5/6 * Math.PI;
				}
			}

			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);
		}

		if( bone.name === 'Right_Lower_Leg' || bone.name === 'Left_Lower_Leg' ) {
			
			euler.setFromQuaternion(bone.quaternion, 'XYZ', true);
			this.API.eulerCanonicalSet(euler);
			euler.y = 0;
			euler.z = 0;
			if( euler.x > 0){
				euler.x = 0;
			}
			quaternion.setFromEuler(euler);
			bone.quaternion.copy(quaternion);

		}

	}

	function api_eulerCanonicalSet(euler){

		if( !euler.isEuler ){
			return;
		}

		// heading
		if ( euler.order[0] === 'X' ){
			if( euler.x < -Math.PI ){
				euler.x = -Math.PI;
			}else if( euler.x > Math.PI ){
				euler.x = Math.PI;
			}
		}else if ( euler.order[0] === 'Y' ){
			if( euler.y < -Math.PI ){
				euler.y = -Math.PI;
			}else if( euler.y > Math.PI ){
				euler.y = Math.PI;
			}
		}else if ( euler.order[0] === 'Z' ){
			if( euler.z < -Math.PI ){
				euler.z = -Math.PI;
			}else if( euler.z > Math.PI ){
				euler.z = Math.PI;
			}
		}

		// picchi
		if ( euler.order[1] === 'X' ){
			if( euler.x < -Math.PI/2 ){
				euler.x = -Math.PI/2;
			}else if( euler.x > Math.PI/2 ){
				euler.x = Math.PI/2;
			}
		}else if ( euler.order[1] === 'Y' ){
			if( euler.y < -Math.PI/2 ){
				euler.y = -Math.PI/2;
			}else if( euler.y > Math.PI/2 ){
				euler.y = Math.PI/2;
			}
		}else if ( euler.order[1] === 'Z' ){
			if( euler.z < -Math.PI/2 ){
				euler.z = -Math.PI/2;
			}else if( euler.z > Math.PI/2 ){
				euler.z = Math.PI/2;
			}
		}

		// bank
		if ( euler.order[2] === 'X' ){
			if( euler.x < -Math.PI ){
				euler.x = -Math.PI;
			}else if( euler.x > Math.PI ){
				euler.x = Math.PI;
			}
		}else if ( euler.order[2] === 'Y' ){
			if( euler.y < -Math.PI ){
				euler.y = -Math.PI;
			}else if( euler.y > Math.PI ){
				euler.y = Math.PI;
			}
		}else if ( euler.order[2] === 'Z' ){
			if( euler.z < -Math.PI ){
				euler.z = -Math.PI;
			}else if( euler.z > Math.PI ){
				euler.z = Math.PI;
			}
		}
	}

}).apply({});
