/*
 *   This file is part of Shenpoo.
 *   By: Kion, LemonHaze420 with contributions from PhilzYeah
 *
 *   Shenpoo is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Shenpoo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Shenpoo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

"use strict";

class Mt5Loader {

	constructor(mats, data) {

		this.ofs = 0;

		this.bones = [];
		this.skel = [];

		this.mats = mats;
		this.data = data;
		this.vertexOfs = 0;
		this.vertex_lookup = [];
		this.view = new DataView(data);
		this.remove_rot = false;
		this.geometry = new THREE.Geometry();

	}

    getBoneName(ID) {
		for(let key in Mt5Names) {
			let boneDef = Mt5Names[key];
			if(boneDef.mt5 !== ID) {
				continue;
			}
			return key;
		}

		return 'Unknown';
    }

	parse() {

		this.header = {
			magic : this.view.getUint32(0x00, true),
			textureOfs : this.view.getUint32(0x04, true),
			modelOfs : this.view.getUint32(0x08, true)
		};
		
		this.ofs = this.header.modelOfs;
		this.readBone();

		this.geometry.computeFaceNormals();
		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(this.geometry);
		buffer.computeBoundingSphere();
		
		let mesh = new THREE.SkinnedMesh( buffer, this.mats);

		if(!this.remove_rot) {
			let skeleton = new THREE.Skeleton( this.bones );
			mesh.add(skeleton.bones[0]);
			mesh.bind( skeleton );
		} else {
			let skeleton = new THREE.Skeleton( this.skel );
			mesh.add(skeleton.bones[0]);
			mesh.bind( skeleton );
		}

		return mesh;

	}

	readBone(parentBone) {

		const c = (2 * Math.PI / 0x10000);
		const node = {
			flag : this.view.getUint32(this.ofs + 0x00, true),
			polygonOfs : this.view.getUint32(this.ofs + 0x04, true),
			rot : {
				x : this.view.getInt32(this.ofs + 0x08, true) * c,
				y : this.view.getInt32(this.ofs + 0x0c, true) * c,
				z : this.view.getInt32(this.ofs + 0x10, true) * c
			},
			scl : {
				x : this.view.getFloat32(this.ofs + 0x14, true),
				y : this.view.getFloat32(this.ofs + 0x18, true),
				z : this.view.getFloat32(this.ofs + 0x1c, true)
			},
			pos : {
				x : this.view.getFloat32(this.ofs + 0x20, true),
				y : this.view.getFloat32(this.ofs + 0x24, true),
				z : this.view.getFloat32(this.ofs + 0x28, true)
			},
			childOfs : this.view.getUint32(this.ofs + 0x2c, true),
			siblingOfs : this.view.getUint32(this.ofs + 0x30, true),
			parentBoneId : this.view.getUint32(this.ofs + 0x34, true),
			unknown_a : this.view.getUint32(this.ofs + 0x38, true),
			unknown_b : this.view.getUint32(this.ofs + 0x3c, true)
		};

		node.id = node.flag & 0xFF;
		if(!parentBone) {
			node.id = 0;
		}
		
		if(this.bones.length === 1 && this.remove_rot) {
			node.pos.y = 1.1;
		}

		this.bone = new THREE.Bone();
		let bone = new THREE.Bone();

		bone.name = this.getBoneName(node.id);
		this.bone.name = this.getBoneName(node.id);

		this.bone.userData = this.bone.userData || {};
		this.bone.userData.id = this.bones.length;
		this.bone.userData.rot = node.rot;

		bone.userData = bone.userData || {}
		bone.userData.ref = this.bone;
		bone.userData.id = this.bones.length;
		bone.userData.rot = node.rot;

		this.skel.push(bone);
		this.bones.push(this.bone);

		// Scale

		this.bone.scale.x = node.scl.x;
		this.bone.scale.y = node.scl.y;
		this.bone.scale.z = node.scl.z;

		// Rotation

		for(let axis in node.rot) {
			let mat = new THREE.Matrix4();
			switch(axis) {
			case 'x':
				mat.makeRotationX(node.rot[axis]);
				break;
			case 'y':
				mat.makeRotationY(node.rot[axis]);
				break;
			case 'z':
				mat.makeRotationZ(node.rot[axis]);
				break;
			}
			this.bone.applyMatrix4(mat);
		}

		// Position

		this.bone.position.x = node.pos.x;
		this.bone.position.y = node.pos.y;
		this.bone.position.z = node.pos.z;

		if(parentBone) {
			parentBone.add(this.bone);
		}

		this.bone.updateMatrix();
		this.bone.updateMatrixWorld();

		if(parentBone) {
			let p = this.skel[parentBone.userData.id];
			p.add(bone);

			const pb = new THREE.Vector3();
			const cb = new THREE.Vector3();
			pb.applyMatrix4(parentBone.matrixWorld);
			cb.applyMatrix4(this.bone.matrixWorld);

			bone.position.x = cb.x - pb.x;
			bone.position.y = cb.y - pb.y;
			bone.position.z = cb.z - pb.z;

			bone.updateMatrix();
			bone.updateMatrixWorld();
		}

		// Read Attached Mesh
			
		if(node.polygonOfs) {
			this.ofs = node.polygonOfs;
			this.readPolygon();
		}

		// Read Child and Sibling Nodes

		if(node.childOfs) {
			this.ofs = node.childOfs;
			this.readBone(this.bone);
		}

		if(node.siblingOfs) {
			this.ofs = node.siblingOfs;
			this.readBone(parentBone);
		}


	}

	readPolygon() {

		// Read Polygon Struct

		const polygon = {
			flag : this.view.getUint32(this.ofs + 0x00, true),
			vertexOfs  : this.view.getUint32(this.ofs + 0x04, true),
			vertexCount : this.view.getUint32(this.ofs + 0x08, true),
			stripOfs : this.view.getUint32(this.ofs + 0x0c, true),
			center : {
				x : this.view.getFloat32(this.ofs + 0x10, true),
				y : this.view.getFloat32(this.ofs + 0x14, true),
				z : this.view.getFloat32(this.ofs + 0x18, true)
			},
			radius : this.view.getFloat32(this.ofs + 0x1c, true)
		};

		// Read Weighted Vertices

		this.ofs = polygon.vertexOfs;
		for(let i = 0; i < polygon.vertexCount; i++) {

			const pos = {
				x : this.view.getFloat32(this.ofs + 0x00, true),
				y : this.view.getFloat32(this.ofs + 0x04, true),
				z : this.view.getFloat32(this.ofs + 0x08, true)
			}

			const norm = {
				x : this.view.getFloat32(this.ofs + 0x0c, true),
				y : this.view.getFloat32(this.ofs + 0x10, true),
				z : this.view.getFloat32(this.ofs + 0x14, true)
			}

			this.ofs += 0x18;
			const vertex = new THREE.Vector3();
			vertex.x = pos.x;
			vertex.y = pos.y;
			vertex.z = pos.z;
			vertex.applyMatrix4(this.bone.matrixWorld);
			this.vertex_lookup[i] = this.geometry.vertices.length;

			let w = new THREE.Vector4(1, 0, 0, 0);
			this.geometry.skinWeights.push(w);
				
			let id = this.bone.userData.id;
			let index = new THREE.Vector4(id, 0, 0, 0);
			this.geometry.skinIndices.push(index);
			this.geometry.vertices.push(vertex);

		}

		if(polygon.stripOfs) {
			this.ofs = polygon.stripOfs;
			this.readStrips(polygon);
		}

		this.vertexOfs += polygon.vertexCount;
		this.bone.userData.vertexOfs = this.vertexOfs;

	}

	readStrips(polygon) {

		// Read Strips

		let vertexOfs = 0;
		if(this.bone.parent) {
			vertexOfs = this.bone.parent.userData.vertexOfs;
		}

		this.ofs = polygon.stripOfs;

		let texId;
		do {

			let stripType = this.view.getUint16(this.ofs, true);
			this.ofs += 2;

			if(stripType === 0x0000 || stripType === 0xffff) {
				continue;
			} else if(stripType === 0x0e) {

				// Material Definition

				let byteLen = this.view.getUint16(this.ofs, true);
				this.ofs += 2;

				// Diffuse ? Ambient ?
				this.ofs += byteLen;

			} else if(stripType === 0x02) {

				let byteLen = this.view.getUint16(this.ofs, true);
				this.ofs += 2;

				// Unknown

				this.ofs += byteLen;

			} else if(stripType === 0x8000) {
				break;

			} else if(stripType === 0x11) {

				let byteLen = this.view.getInt16(this.ofs, true);
				this.ofs += 2;

				let stripCount = this.view.getInt16(this.ofs, true);
				this.ofs += 2;

			for(let i = 0; i < stripCount; i++) {

				let strip = [];
				let stripLen = this.view.getInt16(this.ofs, true);
				this.ofs += 2;

				for(let i = 0; i < Math.abs(stripLen); i++) {
					let idx = this.view.getInt16(this.ofs, true);
					this.ofs += 2;

					let index;
					if(idx >= 0) {
						index = idx + this.vertexOfs;
					} else {
						index = idx + vertexOfs;
					}

					let u = this.view.getInt16(this.ofs, true);
					this.ofs += 2;

					let v = this.view.getInt16(this.ofs, true);
					this.ofs += 2;

					strip.push ({
						i : index,
						u : u / 0x3ff,
						v : v / 0x3ff,
					});
				}

				let clockwise = stripLen < 0;
				for(let i = 0; i < strip.length - 2; i++) {
					let a, b, c;

					if(clockwise && i % 2 === 0) {
						a = strip[i + 1];
						b = strip[i + 0];
						c = strip[i + 2];
					} else {
						a = strip[i + 0];
						b = strip[i + 1];
						c = strip[i + 2];
					}

					let face = new THREE.Face3(a.i, b.i, c.i);
					face.materialIndex = texId;
					this.geometry.faces.push(face);

					let auv = new THREE.Vector2(a.u, a.v);
					let buv = new THREE.Vector2(b.u, b.v);
					let cuv = new THREE.Vector2(c.u, c.v);
					this.geometry.faceVertexUvs[0].push([ auv, buv, cuv ]);

				}

			}

			} else if(stripType === 0x09) {

				texId = this.view.getUint16(this.ofs, true);
				this.ofs += 2;

			} else if(stripType === 0x0b) {

				let uvRatio = this.view.getUint16(this.ofs, true);
				this.ofs+= 2;

			} else if(stripType === 0x08) {

				let flag = this.view.getUint16(this.ofs, true);

			} if(stripType === 0x03) {

				let byteLen = this.view.getUint16(this.ofs, true);
				this.ofs += 2;
				this.ofs += byteLen;

			}

		} while(this.ofs < polygon.vertexOfs);

		this.polygonCount++;


	}

}
