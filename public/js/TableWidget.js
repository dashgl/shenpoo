/*
 *   This file is part of Shenpoo.
 *   By: Kion, LemonHaze420 with contributions from PhilzYeah
 *
 *   Shenpoo is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Shenpoo is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Shenpoo.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


"use strict";

const TableWidget = (function() {

	this.MEM = {}

	this.DOM = {
		thead : document.getElementById('TableWidget.thead'),
		tbody : document.getElementById('TableWidget.tbody')
	}

	this.EVT = {}

	this.API = {
		getBoneName : api_getBoneName.bind(this),
		fillInValues : api_fillInValues.bind(this),
		interpolateLinear : api_interpolateLinear.bind(this),
		interpolateBezier : api_interpolateBezier.bind(this),
		renderTable : api_renderTable.bind(this),
		getFrameData : api_getFrameData.bind(this)
	}

	init.apply(this);
	return this;

	function init() {


	}

	function api_getBoneName(id, type) {

		for(let key in MotnNames) {
			if(MotnNames[key].id !== id) {
				continue;
			}

			if(MotnNames[key].type !== type) {
				continue;
			}

			let bone = MotnNames[key];
			bone.name = key;
			return bone;
		}

		return null;

	}


	function api_fillInValues (length, motn) {
		
		this.MEM.length = length;
		this.MEM.motn = motn;
		PreviewWidget.API.setFrameCount(length);
		
		// First we go ahead and flatten everything

		let flat = new Array();
		this.MEM.motn.forEach( mot => {
			
			let bone = this.API.getBoneName(mot.motn_id, mot.type);

			mot.axis.forEach( d => {
				
				let f = {
					motn_id : mot.motn_id,
					type : mot.type,
					axis : d,
					frames : mot[d],
					bone : bone
				};

				flat.push(f);

			});

		});
		
		this.MEM.flat = flat;
		this.API.interpolateLinear();
		
		// Fill in Debug Values
		
		this.API.renderTable();

		let frameData = this.API.getFrameData(0);
		PreviewWidget.API.setFrameState(frameData);

	}

	function api_interpolateLinear() {

		this.MEM.flat.forEach(flat => {
			
			let array = new Array(this.MEM.length);
			for(let i = 0; i < flat.frames.length; i++) {
				array[flat.frames[i].frame] = flat.frames[i].value;
			}
			
			let val_count = 0;
			for(let i = 0; i < array.length; i++) {
				if(typeof array[i] === 'undefined') {
					continue;
				}
				val_count++;
			}

			if(val_count < 2) {
				return;
			}
			
			while(1) {
				
				let a, b;
				for(let i = 0; i < array.length; i++) {
					if(typeof(array[i]) !== 'undefined') {
						a = {
							index : i,
							value : array[i]
						}
					} else if(typeof(array[i]) === 'undefined' && a) {
						break;
					}
				}

				if(!a) {
					break;
				}

				for(let i = a.index + 1; i < array.length; i++) {
					if(typeof(array[i]) === 'undefined') {
						continue;
					}

					b = {
						index : i,
						value : array[i]
					}
					break;
				}
				
				if(!b) {
					break;
				}

				let f_diff = b.index - a.index;
				let v_diff = b.value - a.value;
				let diff = v_diff / f_diff;
				let val = a.value;

				for(let i = a.index; i < b.index; i++) {
					array[i] = val;
					val += diff;
				}

			}

			
			let clean = [];
			for(let i = 0; i < array.length; i++) {
				clean.push({
					frame : i,
					val : array[i]
				});
			}

			for(let i = 0; i < flat.frames.length; i++) {

				if('value' in flat.frames[i]) {
					clean[flat.frames[i].frame].value = flat.frames[i].value;
				}

				if('easing' in flat.frames[i]) {
					clean[flat.frames[i].frame].easing = flat.frames[i].easing;
				}

			}
			
			flat.frames = clean;

		});

	}

	function api_interpolateBezier() {

		/** Credit : LemonHaze

		struct struct_FCVKey
		{
		  float timeVal;
		  short left_slope;
		  short right_slope;
		  short value;
		};
		
		// ease between 2 FCVKeys
		float __fastcall MOTN::Ease(struct_FCVKey *inData, struct_FCVKey *outData)
		{
		  float v2; // xmm2_4
		  uint32_t v3; // eax
		  float inData_right_slope_1; // xmm6_4
		  float outData_left_slope; // xmm9_4
		  uint32_t v6; // eax
		  float outData_keyframeVal; // xmm8_4
		  uint32_t v8; // eax
		  float inData_timeVal_1; // xmm0_4
		  float inData_keyframeValue; // xmm7_4
		  unsigned int inData_right_slope; // eax
		  float outData_timeVal_inData_diff; // xmm11_4
		  float timeVal_diff; // xmm10_4
		  float timeVal_norm; // xmm2_4

		  LOWORD(v3) = outData->left_slope;
		  inData_right_slope_1 = 0.0;
		  if ( v3 )
		  {
			v3 = v3;
			if ( (v3 & 0x8000u) != 0 )
			  v3 = v3 & 0x7FFF | 0xFFFC0000;
			LODWORD(outData_left_slope) = (v3 << 13) + 0x38000000;
		  }
		  else
		  {
			outData_left_slope = 0.0;
		  }
		  LOWORD(v6) = outData->value;
		  if ( v6 )
		  {
			v6 = v6;
			if ( (v6 & 0x8000u) != 0 )
			  v6 = v6 & 0x7FFF | 0xFFFC0000;
			LODWORD(outData_keyframeVal) = (v6 << 13) + 0x38000000;
		  }
		  else
		  {
			outData_keyframeVal = 0.0;
		  }
		  LOWORD(v8) = inData->value;
		  inData_timeVal_1 = inData->timeVal;
		  if ( v8 )
		  {
			v8 = v8;
			if ( (v8 & 0x8000u) != 0 )
			  v8 = v8 & 0x7FFF | 0xFFFC0000;
			LODWORD(inData_keyframeValue) = (v8 << 13) + 0x38000000;
		  }
		  else
		  {
			inData_keyframeValue = 0.0;
		  }
		  LOWORD(inData_right_slope) = inData->right_slope;
		  if ( inData_right_slope )
		  {
			inData_right_slope = inData_right_slope;
			if ( (inData_right_slope & 0x8000u) != 0 )
			  inData_right_slope = inData_right_slope & 0x7FFF | 0xFFFC0000;
			LODWORD(inData_right_slope_1) = (inData_right_slope << 13) + 0x38000000;
		  }
		  outData_timeVal_inData_diff = outData->timeVal - inData_timeVal_1;
		  timeVal_diff = (v2 - inData_timeVal_1) / outData_timeVal_inData_diff;
		  timeVal_norm = (timeVal_diff * timeVal_diff) * timeVal_diff;
		  return (((((timeVal_norm - ((timeVal_diff * timeVal_diff) + (timeVal_diff * timeVal_diff))) + timeVal_diff)
				  * inData_right_slope_1)
				 + ((timeVal_norm - (timeVal_diff * timeVal_diff)) * outData_left_slope))
				* outData_timeVal_inData_diff)
			   + (((((timeVal_norm + timeVal_norm) - ((timeVal_diff * timeVal_diff) * 3.0)) + 1.0) * inData_keyframeValue)
				+ ((((timeVal_diff * timeVal_diff) * 3.0) - (timeVal_norm + timeVal_norm)) * outData_keyframeVal));
		}
		**/

	}

	function api_renderTable() {

		this.DOM.thead.innerHTML = '';
		this.DOM.tbody.innerHTML = '';

		// First we create the header

		let rh = this.DOM.thead.insertRow();
		let h0 = rh.insertCell();
		let h1 = rh.insertCell();
		let h2 = rh.insertCell();
		let h3 = rh.insertCell();
		
		h0.textContent = 'MTON';
		h1.textContent = 'MT5';
		h2.textContent = 'Axis';
		h3.textContent = 'Type';

		for(let i = 0; i < this.MEM.length; i++) {
			rh.insertCell().textContent = i;
		}

		this.MEM.flat.forEach( flat => {

			let row = this.DOM.tbody.insertRow();
			let c0, c1, c2, c3;

			if(flat.axis === 'x') {
				c0 = row.insertCell();
				c1 = row.insertCell();
				c2 = row.insertCell();
				c3 = row.insertCell();
			} else {
				c1 = row.insertCell();
				c2 = row.insertCell();
			}
			
			if(flat.bone) {
				c1.textContent = flat.bone[flat.axis];
			}
			c2.textContent = flat.axis;

			if(flat.axis === 'x') {

				if(flat.bone) {
					let parts = flat.bone.name.split(' ');
					for(let i = 0; i < parts.length; i++) {
						let a = document.createElement('span');
						a.textContent = parts[i];
						c0.appendChild(a);
					}
				}
				
				let b = document.createElement('span');
				b.textContent = flat.motn_id;
				c0.appendChild(b);

				c3.textContent = flat.type.toUpperCase();
				c0.setAttribute('rowspan', '3');
				c3.setAttribute('rowspan', '3');

				if(flat.bone) {
					let c = document.createElement('span');
					c.textContent = flat.bone.format;
					c3.appendChild(c);

					if(flat.bone.format === 'IK') {
						c3.classList.add('ik');
					}
				}

			}


			for(let i = 0; i < this.MEM.length; i++) {
				let cell = row.insertCell();

				if(flat.axis === 'x') {
					cell.classList.add('dash-x');
				} else if(flat.axis === 'y') {
					cell.classList.add('dash-y');
				}
				
				let found;
				for(let k = 0; k < flat.frames.length; k++) {
					if(i !== flat.frames[k].frame) {
						continue;
					}
					found = flat.frames[k];
					break;
				}

				if(!found) {
					continue;
				}
				
				if(found.hasOwnProperty('value')) {
					if(flat.type === 'pos') {
						cell.textContent = found.value.toFixed(2);
					} else {
						let rad = found.value % (Math.PI*2);
						cell.textContent = rad.toFixed(2);
					}
					cell.classList.add('val');
				} else if(found.hasOwnProperty('val')) {
				
					if(flat.type === 'pos') {
						cell.textContent = found.val.toFixed(2);
					} else {
						let rad = found.val % (Math.PI*2);
						cell.textContent = rad.toFixed(2);
					}

				}

				if(found.hasOwnProperty('easing')) {
					let str = [];
					str.push(found.easing[0].toFixed(2));
					if(found.easing[1]) {
						str.push(found.easing[1].toFixed(2));
					}
					cell.setAttribute('title', '(' + str.join(',') + ')');
					cell.classList.add('ease');
				}

			}

		});

	}

	function api_getFrameData(index) {

		let data = {};

		this.MEM.flat.forEach( flat => {
			
			let key = `motn_${flat.motn_id}`;
			data[key] = data[key] || {};
			data[key][flat.type] = data[key][flat.type] || {};
			data[key][flat.type][flat.axis] = null;

			for(let i = 0; i < flat.frames.length; i++) {
				if(flat.frames[i].frame !== index) {
					continue;
				}

				if('value' in flat.frames[i]) {
					data[key][flat.type][flat.axis] = flat.frames[i].value;
				} else if('val' in flat.frames[i]) {
					data[key][flat.type][flat.axis] = flat.frames[i].val;
				}

				break;
			}

		});

		return data;

	}

}).apply({});
