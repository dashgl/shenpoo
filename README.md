# Shenmue IK Debugger

Shenmue IK Debugger is an online tool for debugging Shenmue animations using Threejs. Liver version can be found [here](https://dashgl.gitlab.io/shenpoo)

## How to Use

![Shenpoo Tab 1](./figs/Shenpoo-tab1.png)


## Status

- [x] Implement Model Loader
- [x] Reverse Enineer Animation Format
- [x] Implement Feet Targets
- [x] Implement Hand Targets
- [ ] Implement Mixed FK animations
- [ ] Implement Torso IK Target
- [ ] Implement Look At IK Target
- [ ] Implement Bezier Interpolation

## License

Source code is available under GPLv3 license.
